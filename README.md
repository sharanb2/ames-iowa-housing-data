Files : 
-Ames_IOWA_houingprice_predictor.r : The main file
-Ames_data.csv - datafile

Download the data set and set the working directory to the folder which contains the dataset.
Run the code

Information about pre-processing and Implementation is given below
1. Preprocessing:
• NA’s were removed from the data by assigning 0 to each occurrence of NA.
• Irrelevant variables were removed from the data
• ‘Mo_Sold’ and ‘Year_Sold’ were changed to categorical variables
• Each Categorical variable was converted to One-Hot vector
• Numerical Variables are windsorized to decrease skewness by limiting the values greater than 95 percentile of the training data to 95th percentile
• Windorized numerical variables were converted to log scale, where some of the variables are added with 1 before log transformation to avoid log transformation of 0s.
• The response variable ‘Sale Price’ was converted to log scale.
• For GAM, some numerical variables were chosen to be linear predictors, and some were chosen to be non-linear predictors. Levels of some categorical variables (selected from the Elastic Net output output) were chosen to be represented as binary variables

2. Implementation:
• Linear Regression Model with Elastic Penalty was used to develop a model with hyper parameter alpha = 0.5. Elastic penalty gave better results compared to only Lasso (alpha = 1) or Ridge (alpha=0) penalties. This was performed using glmnet package in R.
• Among the tree models, Boosting was implemented using XGBoost package in R. The following hyper parameters were used, which were chosen after trying with different values for the hyper parameters:
o Booster = gbtree
o Maximum Tree depth = 4
o Number of trees = 450
o Objective = reg:linear
o Learning Rate = 0.05
• GAM model was developed using mgcv package in R.
o No. of Linear predictors: 9
o No. of Non-Linear predictors: 20
o No. of Binary Features: 60